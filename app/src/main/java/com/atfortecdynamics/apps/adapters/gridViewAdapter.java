package com.atfortecdynamics.apps.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.atfortecdynamics.apps.DO.gridItem;
import com.atfortecdynamics.apps.R;

import java.util.List;

/**
 * Created by folio on 10/5/2018.
 */

public class gridViewAdapter extends BaseAdapter {

    Context context;
    List<gridItem> data;
    LayoutInflater inflater;

    public gridViewAdapter(Context context, List<gridItem> data) {
        this.context = context;
        this.data = data;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.custom_grid_item,null);
        ImageView imageView = view.findViewById(R.id.img_item);
        TextView textView = view.findViewById(R.id.txt_item);

        imageView.setImageResource(data.get(i).getImage());
        textView.setText(data.get(i).getItemName());
        return view;
    }
}

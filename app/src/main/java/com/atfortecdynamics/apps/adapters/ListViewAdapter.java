package com.atfortecdynamics.apps.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.atfortecdynamics.apps.DO.BillPayment;
import com.atfortecdynamics.apps.R;

import java.util.List;

/**
 * Created by folio on 10/11/2018.
 */

public class ListViewAdapter extends BaseAdapter {

    Context context;
    List<BillPayment> data;
    LayoutInflater inflater;

    public ListViewAdapter(Context context, List<BillPayment> data) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.custom_row_list_view,null);
        TextView tx_acc_no = view.findViewById(R.id.txt_acc_no);
        TextView tx_amount = view.findViewById(R.id.txt_amount);
        TextView tx_utility = view.findViewById(R.id.txt_utility);

        tx_acc_no.setText(data.get(i).getAccountNumber());
        tx_amount.setText(data.get(i).getAmount());
        tx_utility.setText(data.get(i).getUtilityType());

        return view;
    }
}

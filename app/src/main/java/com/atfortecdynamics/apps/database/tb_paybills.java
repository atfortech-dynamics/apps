package com.atfortecdynamics.apps.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.atfortecdynamics.apps.DO.BillPayment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by folio on 10/9/2018.
 */

public class tb_paybills extends controller {
    public tb_paybills(Context context) {
        super(context);
    }

    public static final String tableName = "tb_paybills";
    public static final String col_1="id";
    public static final String col_2="accountNumber";
    public static final String col_3="amount";
    public static final String col_4="utilityType";


    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS tb_paybills(" +
            "id INT(11)," +
            "accountNumber VARCHAR(255)," +
            "amount INT (11)," +
            "utilityType VARCHAR(255))";

    public static final String DROP_TABLE ="DROP TABLE IF EXISTS tb_paybills";

    public void insertRecord(BillPayment billPayment){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(col_2,billPayment.getAccountNumber());
        values.put(col_3,billPayment.getAmount());
        values.put(col_4,billPayment.getUtilityType());

        db.insert(tableName,null,values);
        db.close();
    }

    public List<BillPayment> fetchRecords(){
        SQLiteDatabase db = getReadableDatabase();
        String sqlStatement = "SELECT * FROM "+tableName;
        List<BillPayment> data  = new ArrayList<>();
        Cursor cursor=null;

        // adding data to the cursor before retrieving to the list
        cursor = db.rawQuery(sqlStatement,null);

        // check whether the cursor has data or not

        if(cursor.moveToFirst()){

            //cursor contains data. create a loop to enable us read  data from the cursor
            do{
                BillPayment row = new BillPayment();
                String accNo = cursor.getString(cursor.getColumnIndex(col_2));
                String amount = cursor.getString(cursor.getColumnIndex(col_3));
                String utilityType = cursor.getString(cursor.getColumnIndex(col_4));

                row.setAccountNumber(accNo);
                row.setAmount(amount);
                row.setUtilityType(utilityType);

                data.add(row);
            }while (cursor.moveToNext());

        }

        // closing the database prevents data and memory leakage in the mobile
        db.close();

        return data;

    }

}

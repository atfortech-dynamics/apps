package com.atfortecdynamics.apps.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by folio on 10/9/2018.
 */

public class controller extends SQLiteOpenHelper {

    public static  final String dbName="db_bills";
    public controller(Context context) {
        super(context, dbName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL(tb_paybills.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
            db.execSQL(tb_paybills.DROP_TABLE);
    }
}

package com.atfortecdynamics.apps;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.atfortecdynamics.apps.DO.BillPayment;
import com.atfortecdynamics.apps.adapters.ListViewAdapter;
import com.atfortecdynamics.apps.database.tb_paybills;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by folio on 10/11/2018.
 */

public class Transcations extends AppCompatActivity {

    List<BillPayment> data = new ArrayList<>();
    tb_paybills tablePaybills;
    ListView listView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_transcations);

        listView = findViewById(R.id.list_view_transcations);
        tablePaybills = new tb_paybills(getApplicationContext());

        data = tablePaybills.fetchRecords();

        ListViewAdapter adapter = new ListViewAdapter(getApplicationContext(),data);

        listView.setAdapter(adapter);

    }
}

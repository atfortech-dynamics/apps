package com.atfortecdynamics.apps;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.atfortecdynamics.apps.network.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by folio on 10/4/2018.
 */

public class registration extends Activity {



    TextView txt_reg;
    Button btn_sign_up;

    EditText fname, lname, id, password, rpassword;
    String strfname, strlname, strid, strpassword, strrpassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);

        txt_reg = findViewById(R.id.txt_reg);
        btn_sign_up = findViewById(R.id.sign_up);
        fname=findViewById(R.id.fname);
        lname=findViewById(R.id.lname);
        id=findViewById(R.id.id);
        password=findViewById(R.id.password);




        txt_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(registration.this, login.class);
                startActivity(intent);
            }
        });
        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                strfname = fname.getText().toString();
                strlname = lname.getText().toString();
                strid = id.getText().toString();
                strpassword = password.getText().toString();
//                strrpassword = rpassword.getText().toString();


                new registrationProcessor().execute();
            }
        });
    }

    public class registrationProcessor extends AsyncTask<String, String, String> {

        int status;
        String message;

        ProgressDialog dialog = new ProgressDialog(registration.this);
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setMessage("Authenticating. Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONParser jsonParser = new JSONParser();
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("username", strfname));
            params.add(new BasicNameValuePair("email", strlname));
            params.add(new BasicNameValuePair("phone_number", strid));
            params.add(new BasicNameValuePair("password", strpassword));
            params.add(new BasicNameValuePair("id_no", strrpassword));

            JSONObject jsonObject = jsonParser.RequestObject("http://192.168.0.14/" +
                    "www.android-payments.com/insert-user.php", params);
            try {
                status = jsonObject.getInt("status");
                message = jsonObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();

            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

        }
    }
}

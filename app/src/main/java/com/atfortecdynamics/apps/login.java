package com.atfortecdynamics.apps;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.atfortecdynamics.apps.network.JSONParser;
import com.atfortecdynamics.apps.network.Requestor;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by folio on 10/4/2018.
 */

public class login extends Activity {

    TextView txt_not_reg;
    Button btn_sign_in;

    EditText username,password;
    String strUsername,strPassword;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        username = findViewById(R.id.name);
        password = findViewById(R.id.password);



        txt_not_reg = findViewById(R.id.txt_not_reg);
        btn_sign_in = findViewById(R.id.signin);
        txt_not_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (login.this,registration.class);
                startActivity(intent);
            }
        });

        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent (login.this,HomePage.class);
//                startActivity(intent);

                strPassword=password.getText().toString();
                strUsername=username.getText().toString();

                new loginProcessor().execute();
            }
        });
    }

    public class loginProcessor extends AsyncTask<String,String,String>{

        int status;
        String message;

        ProgressDialog dialog = new ProgressDialog(login.this);
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setMessage("Authenticating. Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected String doInBackground(String... strings) {

            JSONParser jsonParser = new JSONParser();
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("email",strUsername));
            params.add(new BasicNameValuePair("password",strPassword));
            JSONObject jsonObject = jsonParser.RequestObject("http://192.168.0.14:80/" +
                    "www.android-payments.com/checkuser.php",params);

            try {
                status = jsonObject.getInt("status");
                message = jsonObject.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s){
            super.onPostExecute(s);
            dialog.dismiss();

            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
        }
    }
}

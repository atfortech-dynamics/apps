package com.atfortecdynamics.apps;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.atfortecdynamics.apps.DO.gridItem;
import com.atfortecdynamics.apps.adapters.gridViewAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by folio on 10/5/2018.
 */

public class HomePage extends AppCompatActivity {

    List<gridItem> data = new ArrayList<>();
    List<gridItem> data_purchases = new ArrayList<>();
    GridView gridPayBills,gridPurchases;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        data.add(new gridItem(R.mipmap.img_power,"ELECTRICITY"));
        data.add(new gridItem(R.mipmap.img_water,"WATER"));
        data.add(new gridItem(R.mipmap.img_phone,"MOBILE"));
        data.add(new gridItem(R.mipmap.img_call,"LANDLINE"));
        data.add(new gridItem(R.mipmap.img_cable_tv,"CABLE TV"));
        data.add(new gridItem(R.mipmap.img_internet,"INTERNET"));

        data_purchases.add(new gridItem(R.mipmap.img_movies,"MOVIES"));
        data_purchases.add(new gridItem(R.mipmap.img_events,"EVENT"));
        data_purchases.add(new gridItem(R.mipmap.img_sport,"SPORT"));

        gridPayBills = findViewById(R.id.grid_pay_bills);
        gridPurchases = findViewById(R.id.grid_purchase_items);
        gridViewAdapter adapter = new gridViewAdapter(getApplicationContext(),data);
        gridPayBills.setAdapter(adapter);

        gridViewAdapter adapter1 = new gridViewAdapter(getApplicationContext(),data_purchases);
        gridPurchases.setAdapter(adapter1);

        gridPayBills.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                AppConfig.utilityType = data.get(i).getItemName();
                Intent intent = new Intent(HomePage.this,ActivityPaybills.class);
                startActivity(intent);
            }
        });

    }
}

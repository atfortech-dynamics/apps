package com.atfortecdynamics.apps.DO;

/**
 * Created by folio on 10/9/2018.
 */

public class BillPayment {

    private String accountNumber;
    private String amount;
    private String utilityType;

    public BillPayment(String accountNumber, String amount, String utilityType) {
        this.accountNumber = accountNumber;
        this.amount = amount;
        this.utilityType = utilityType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAmount() {
        return amount;
    }

    public String getUtilityType() {
        return utilityType;
    }

    public BillPayment() {
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setUtilityType(String utilityType) {
        this.utilityType = utilityType;
    }
}

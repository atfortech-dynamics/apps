package com.atfortecdynamics.apps.DO;

/**
 * Created by folio on 10/5/2018.
 */

public class gridItem {
    private int image;
    private String itemName;

    public gridItem(int image, String itemName) {
        this.image = image;
        this.itemName = itemName;
    }

    public gridItem() {
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}

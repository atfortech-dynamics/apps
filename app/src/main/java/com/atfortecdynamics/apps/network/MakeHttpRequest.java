package com.atfortecdynamics.apps.network;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by folio on 10/16/2018.
 */

public class MakeHttpRequest {


    public JSONObject requestJsonObject(String url, List<NameValuePair> params) {
        InputStream inputStream = null;
        String error = "{\"status\":500,\"message\":\"error in network connection\"}";
        JSONObject jsonObject = null;

        String json = null;

        String variables = URLEncodedUtils.format(params,"UTF-8");
        String urlRequestor = url+"?"+variables;

        Log.d("http request",urlRequestor);


        DefaultHttpClient client = new DefaultHttpClient();
        try {
            inputStream = client.execute(new HttpGet(urlRequestor)).
                    getEntity().getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"),8);

            StringBuilder stringBuilder = new StringBuilder();

            String line = reader.readLine();

            while (line==null){
                break;
            }
            stringBuilder.append(line+"\n");

            json = stringBuilder.toString();
            Log.d("http response",json);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            jsonObject = new JSONObject(json);
        } catch (JSONException e) {

            try {
                jsonObject = new JSONObject(error);
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

        return jsonObject;
    }
}

package com.atfortecdynamics.apps.network;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by folio on 10/16/2018.
 */

public class JSONParser {

    public JSONObject RequestObject(String url, List<NameValuePair> params){
        return new MakeHttpRequest().requestJsonObject(url, params);
    }

    public JSONObject RequestObject(String url, JSONObject jsonObject){
        return  null;
    }
}

package com.atfortecdynamics.apps;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.atfortecdynamics.apps.DO.BillPayment;
import com.atfortecdynamics.apps.database.tb_paybills;

/**
 * Created by folio on 10/9/2018.
 */

public class ActivityPaybills extends AppCompatActivity {

    //push test
    EditText ed_accountNumber,ed_amount;
    FloatingActionButton fab_Insert;
    tb_paybills tbPaybills;

    Button btn_view_transcations;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paybill);

        ed_accountNumber=findViewById(R.id.account_number);
        ed_amount=findViewById(R.id.amount);
        fab_Insert = findViewById(R.id.fab_make_payment);
        btn_view_transcations = findViewById(R.id.btn_view_transcations);

        tbPaybills = new tb_paybills(getApplicationContext());

        fab_Insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String acc_no =ed_accountNumber.getText().toString();
                String amnt = ed_amount.getText().toString();

                if(!acc_no.isEmpty() && !amnt.isEmpty()){
                    BillPayment bill = new BillPayment(acc_no,amnt,AppConfig.utilityType);
                    tbPaybills.insertRecord(bill);
                    Toast.makeText(getApplicationContext(),"data inserted successfully",Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(getApplicationContext(),"missing some information",Toast.LENGTH_SHORT).show();


            }
        });

        btn_view_transcations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ActivityPaybills.this,Transcations.class);
                startActivity(intent);
            }
        });
    }
}
